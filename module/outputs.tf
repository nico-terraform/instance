output "sortie_module_instances_adresse_ip" {
  value = libvirt_domain.vm[*].network_interface[*].addresses
}

output "sortie_module_instances_id" {
  value = libvirt_domain.vm[*].id
}
