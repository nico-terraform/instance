variable "name" {}
variable "ip" {}
variable "network_id" {}
variable "pool" {}
variable "nombre_instance" {}
variable "ssh_public_key" {}
variable "mot_de_passe" {}
variable "size" {}
variable "memory" {}
variable "vcpu" {}
variable "vm_image" {
  type    = string
  default = "https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img"
}

