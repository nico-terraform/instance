# On indique les caractéristique de la vm
resource "libvirt_domain" "vm" {
  count = var.nombre_instance
  name = "vm_${var.name}${count.index}"
  memory = "${var.memory}"
  vcpu   = "${var.vcpu}"
  cloudinit = libvirt_cloudinit_disk.commoninit[count.index].id         # Execution du fichier
  network_interface {
    network_id = "${var.network_id}"
    addresses = [
        "${var.ip}.1${count.index}"
        ]
  }
  disk {
    volume_id = libvirt_volume.vm_volume_size[count.index].id
  }
  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }
  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}


