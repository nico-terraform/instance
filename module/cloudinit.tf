# Declaration d'un fichier user_data a executer
data "template_file" "user_data" {
  count = var.nombre_instance
  template = file("${path.module}/cloud_init.cfg")
  vars = {
    hostname = "${var.name}${count.index}"
    ssh_public_key = "${var.ssh_public_key}"
    mot_de_passe = "${var.mot_de_passe}"
  }
}

# Lecture du fichier a executer
resource "libvirt_cloudinit_disk" "commoninit" {
  count = var.nombre_instance
  name = "commoninit_${var.name}${count.index}.iso"
  pool = "${var.pool}"
  user_data = data.template_file.user_data[count.index].rendered
}
