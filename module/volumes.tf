# Création d'un volume pour stocker l'image de l'OS
resource "libvirt_volume" "vm_image" {
  name   = "vm_${var.name}_qcow2"
  pool   = "${var.pool}"
  source = "${var.vm_image}"
  format = "qcow2"
}

resource "libvirt_volume" "vm_volume_size" {
  count = var.nombre_instance
  name   = "vm_size_${var.name}${count.index}"
  base_volume_id  = libvirt_volume.vm_image.id
  pool            = "${var.pool}"
  size            = "${var.size}" * 1073741824
}
