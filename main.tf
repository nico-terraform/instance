variable "instance" {
  type = map
  default = {
    name = "instance"
    nombre_instance = 1
    ip = "20.0.0"
    ssh_public_key = "ecdsa-sha2-nistp521 AAAAE2VjZHNhLXNoYTItbmlzdHA1MjEAAAAIbmlzdHA1MjEAAACFBADI7xPJjC4iY27pijO9Kw/UKlzsznEDtyuZ4qFYxZ+QrKEsrzJ2Q2HsU0aE2UuB12GzV69Msboxc5pwpc/8zvZLRAHiNda8XvNOHVTtgpPNLYDgQ16TurUFFzTzV5vyrgWdvvWl8VY2WR0zz0fAb5RV9419Q+uNwjLv/ecqgg3Msu3s2A== ndf@ndf-tuxedo"
    mot_de_passe = "1dc44d23e82c6514556cdf51c58c2c0486caeabb6362080bb3d1359d3d76b9e7a58c24e51959a0d894d7ecd904ae7df1a3c94a47f72013e83e5f318bb05a5ab1"
    size = 5  # Taille du volume en Gb
    memory = 3072
    vcpu   = 2
  }
}

# Le provider est récuperer sur le réseau
terraform {
  required_providers {
    libvirt = {
      source = "dmacvicar/libvirt"
      version = "0.7.1"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

# Création d'un network
resource "libvirt_network" "terraform_vm_network" {
  name = "${var.instance.name}_vm_network"
  addresses = [
    "${var.instance.ip}.0/16"
    ]
  mode = "nat"
  dhcp {
    enabled = true
  }
}

# Création d'une pool
resource "libvirt_pool" "tabouret_pool" {
  name = "${var.instance.name}_pool"
  type = "dir"
  path = "/var/lib/libvirt/${var.instance.name}_image_pool"
}

module "instance" {
  source = "./module"
  name = var.instance.name
  nombre_instance = var.instance.nombre_instance
  ip = var.instance.ip
  ssh_public_key = var.instance.ssh_public_key
  mot_de_passe = var.instance.mot_de_passe
  size = var.instance.size
  memory = var.instance.memory
  vcpu = var.instance.vcpu
  network_id = libvirt_network.terraform_vm_network.id
  pool = libvirt_pool.tabouret_pool.name
}

